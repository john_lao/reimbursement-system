window.onload = function(){
	getSessionUser();
}

let status = "";
let username;
let userId;
let userRole;
let globalReimbursements;


let form = document.getElementById('reimbursement-form');
// Adds a listener for the "submit" event.
form.addEventListener('submit', function(e) {
	  e.preventDefault();
	  postReimbursement();
	  $('#exampleModal').modal('toggle')
	  $('#exampleModal').on('hidden.bs.modal', function (e) {
		  $(this)
		    .find("input,textarea,select")
		       .val('')
		       .end()
		    .find("input[type=checkbox], input[type=radio]")
		       .prop("checked", "")
		       .end();
		})
});


function getType(e){
	let type = e.value;
	return type;
}

function getStatus(e){
	status = e.value;
	$("#table_body").empty();
	attachedTableElement(globalReimbursements,status,userRole);
}

function attachedTableElement(Reimbursements,status,role){

	let filteredReimbursements;
			if(status && status != "Filter by status"){
				filteredReimbursements = Reimbursements.filter((reimbursement)=>{
				return reimbursement.status === status;
				})
			}else{
				filteredReimbursements = Reimbursements;
			}
			filteredReimbursements.forEach((reimbursement,i)=>{
				let row = document.createElement("TR");
				row.setAttribute("id", "table_row" + i);
				document.getElementById("table_body").appendChild(row);
				  
				let type = document.createElement("TD");
				let status = document.createElement("TD");
				let amount = document.createElement("TD");
				let description = document.createElement("TD");
				let createdDate = document.createElement("TD");
				let resolvedDate = document.createElement("TD");
				let resolvedBy = document.createElement("TD");
				let receipt = document.createElement("TD");
				let username = document.createElement("TD");
				
				
				type.setAttribute("class", "reimbursement_type");
				status.setAttribute("class", "reimbursement_status");
				amount.setAttribute("class", "reimbursement_amount");
				description.setAttribute("class", "reimbursement_description");
				createdDate.setAttribute("class", "reimbursement_createdDate");
				resolvedDate.setAttribute("class", "reimbursement_resolvedDate");
				resolvedBy.setAttribute("class", "reimbursement_resolvedBy");
				receipt.setAttribute("class", "reimbursement_receipt");
				username.setAttribute("class", "reimbursement_username");
				
				type.setAttribute("id", "reimbursement_type" + i);
				status.setAttribute("id", "reimbursement_status" + i);
				amount.setAttribute("id", "reimbursement_amount" + i);
				description.setAttribute("id", "reimbursement_description"+i);
				createdDate.setAttribute("id", "reimbursement_createdDate"+i);
				resolvedDate.setAttribute("id", "reimbursement_resolvedDate" + i);
				resolvedBy.setAttribute("id", "reimbursement_resolvedBy" + i);
				receipt.setAttribute("id", "reimbursement_receipt"+ i);
				username.setAttribute("class", "reimbursement_username" + i);
				  
				let tableHead = document.createElement("TH");
				tableHead.setAttribute("class", "reimbursement_id");
				tableHead.setAttribute("scope", "row");
				tableHead.appendChild(document.createTextNode(reimbursement.id));
				username.appendChild(document.createTextNode(reimbursement.username));
				type.appendChild(document.createTextNode(reimbursement.type));
				status.appendChild(document.createTextNode(reimbursement.status));
				amount.appendChild(document.createTextNode(reimbursement.amount));
				description.appendChild(document.createTextNode(reimbursement.description));
				createdDate.appendChild(document.createTextNode(reimbursement.dateSubmitted));
				if(reimbursement.resolverUsername === null) {
		    		resolvedBy.appendChild(document.createTextNode("request pending"));
				  	resolvedDate.appendChild(document.createTextNode("request pending"));
				}else{
				  	resolvedBy.appendChild(document.createTextNode(reimbursement.resolverUsername));
				  	resolvedDate.appendChild(document.createTextNode(reimbursement.dateResolved));
				}
				  
				receipt.appendChild(document.createTextNode(reimbursement.receipe));
				
				document.getElementById("table_row" + i).appendChild(tableHead);
				document.getElementById("table_row" + i).appendChild(username);
				document.getElementById("table_row" + i).appendChild(type);
				document.getElementById("table_row" + i).appendChild(status);
				document.getElementById("table_row" + i).appendChild(amount);
				document.getElementById("table_row" + i).appendChild(description);
				document.getElementById("table_row" + i).appendChild(createdDate);
				document.getElementById("table_row" + i).appendChild(resolvedDate);
				document.getElementById("table_row" + i).appendChild(resolvedBy);
				document.getElementById("table_row" + i).appendChild(receipt);
					  
				if(role === "finance manager" && reimbursement.status == 'approved'){
						// approve and pending
			  	    let decision = document.createElement("TD");
				    let buttonHtml = `<div style="text-align:center">
							             Case close
							          </div>`
				
				    decision.setAttribute("class", "reimbursement_decision");
				    decision.setAttribute("id", "reimbursement_decision" + i);
				  
				    decision.innerHTML = buttonHtml;
				    document.getElementById("table_row" + i).appendChild(decision);
				}else if (role === "finance manager" && reimbursement.status == 'denied') {
					let decision = document.createElement("TD");
						  let buttonHtml = `<div style="text-align:center">
										<button type="button" class="btn btn-secondary" id="approved-button${i}" value="approved/${reimbursement.id}/${reimbursement.dateSubmitted}" onclick="updateStatus(this)">Approved</button>
									  </div>`
						
						  decision.setAttribute("class", "reimbursement_decision");
						  decision.setAttribute("id", "reimbursement_decision" + i);
						  
						  decision.innerHTML = buttonHtml;
						  document.getElementById("table_row" + i).appendChild(decision);
				}else if (role === "finance manager"){
					
					 var decision = document.createElement("TD");
					 var buttonHtml = `<div>
									  <button type="button" class="btn btn-secondary approved-button" id="approved-button${i}" value="approved/${reimbursement.id}/${reimbursement.dateSubmitted}" onclick="updateStatus(this)">Approved</button>
									  <button type="button" class="btn btn-secondary denied-button" id="denied-button${i}" value="denied/${reimbursement.id}/${reimbursement.dateSubmitted}" onclick="updateStatus(this)">Denied</button>
									  </div>`
						
					decision.setAttribute("class", "reimbursement_decision");
				  	decision.setAttribute("id", "reimbursement_decision" + i);
				  
				  	decision.innerHTML = buttonHtml;
				  	document.getElementById("table_row" + i).appendChild(decision);
					
				}
				});		  
}


function getSessionUser(){
	let xhttp = new XMLHttpRequest();
	
	xhttp.onreadystatechange = function(){
		
		if(xhttp.readyState == 4 && xhttp.status == 200) {
			let user = JSON.parse(xhttp.responseText);
			let capFirstName = user.firstName[0].toUpperCase() + user.firstName.substring(1);
			let capLastName  = user.lastName[0].toUpperCase() + user.lastName.substring(1);
			let capRole = user.role.split(" ");
			for(let i = 0 ; i < capRole.length; i++) {	
				capRole[i] = capRole[i][0].toUpperCase() + capRole[i].substring(1);
			}
			capRole = capRole.join(" ");
			document.getElementById("welcome").innerText = capFirstName + " " + capLastName +" as " + capRole;
			userRole = user.role;
			username = user.username;
			userId = user.id;
			if(userRole ==="finance manager") {
						  var title = document.createElement("TH");
							  title.setAttribute("scope","col");
							  title.appendChild(document.createTextNode("Approved/Denied"))
							  document.getElementById("title-row").appendChild(title);
			}
			getUserReimbursement(userRole);
		}
	}
	
	xhttp.open("GET", "http://localhost:9020/users/session");
	
	xhttp.send();
}

function getUserReimbursement(role){
	let xhttp = new XMLHttpRequest();
	
	xhttp.onreadystatechange = function(){
		
		if(xhttp.readyState == 4 && xhttp.status == 200) {
			let Reimbursements = JSON.parse(xhttp.responseText);
			globalReimbursements = Reimbursements;
			$("#table_body").empty()
			attachedTableElement(Reimbursements,status,userRole);
		}
	}
	
	xhttp.open("GET", "http://localhost:9020/user/reimbursement");
	
	xhttp.send();
}


function updateStatus(e){
	let xhttp = new XMLHttpRequest();
	let changeStatus = e.value.split("/")[0];
	let id = e.value.split("/")[1];
	let dateSubmitted = e.value.split("/")[2];
	xhttp.onreadystatechange = function(){
		
		if(xhttp.readyState == 4 && xhttp.status == 200) {
			getUserReimbursement(userRole);
			
		}
	}
	
	xhttp.open("PUT", "http://localhost:9020/users/reimbursement");
	
	console.log({
		"id":Number(id),
		"status":changeStatus,
		"resolverId":userId,
		"dateResolved":moment(new Date()).format("YYYY-MM-DD HH:mm:ss"),
		"dateSubmitted":dateSubmitted
	})
	xhttp.send(JSON.stringify({
		"id":Number(id),
		"status":changeStatus,
		"resolverId":userId,
		"dateResolved":moment(new Date()).format("YYYY-MM-DD HH:mm:ss"),
		"dateSubmitted":dateSubmitted
	}));
	
}


function postReimbursement(){
	let xhttp = new XMLHttpRequest();
	let amount = document.getElementById("exampleInputAmount").value;
	let description = document.getElementById("exampleInputDescription").value;
	let type = document.getElementById("type-select").value;
	let dateSubmitted = moment(new Date()).format("YYYY-MM-DD HH:mm:ss")
	xhttp.onreadystatechange = function(){
		
		if(xhttp.readyState == 4 && xhttp.status == 200) {
			if(amount <= 0) {
				alert("invalid amount");
			}
			getUserReimbursement(userRole);
		}
	}
	
	xhttp.open("POST", "http://localhost:9020/users/reimbursement");
	
	xhttp.send(
		JSON.stringify({
			amount:amount, 
			description:description, 
			type:type, 
			dateSubmitted:dateSubmitted, 
			authorId:userId
	}));
}	