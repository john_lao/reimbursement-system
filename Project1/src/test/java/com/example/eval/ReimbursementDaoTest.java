package com.example.eval;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.dao.ReimbursementDBConnection;
import com.example.dao.ReimbursementDao;
import com.example.model.Reimbursement;

public class ReimbursementDaoTest {
	private static String url = "jdbc:mariadb://database-1.c7otdgsixoyh.us-east-2.rds.amazonaws.com:3306/fooddb";
	private static String username = "fooduser";
	private static String password = "mypassword";
	
	@Mock
	private	ReimbursementDBConnection rdc;
	
//	@Mock
//	private DriverManager dm;
	
	@Mock
	private Connection c;
	
	@Mock
	private PreparedStatement ps;
	
	@Mock
	private ResultSet rs;
	
	private Reimbursement testReimbursement1;
	private Reimbursement testReimbursement2;
	private Reimbursement testReimbursement3;
	private Reimbursement testReimbursement4;
	
	@BeforeClass
	public static void setUpBeforeClass()  throws Exception {
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(rdc.getDBConnection()).thenReturn(c);
		when(c.prepareStatement(any(String.class))).thenReturn(ps);
		testReimbursement1 = new Reimbursement(1, 5000, "2021-01-09 10:29:16.0", "2021-01-09 19:29:16.0", 
				"food=20 per day, tranportation=50 total", null, 1, "smart", "approved", "food", "shmoo");
		when(rs.first()).thenReturn(true);
		when(rs.getInt(1)).thenReturn(testReimbursement1.getId());
		when(rs.getInt(2)).thenReturn(testReimbursement1.getAmount());
		when(rs.getString(3)).thenReturn(testReimbursement1.getDateSubmitted());
		when(rs.getString(4)).thenReturn(testReimbursement1.getDateResolved());
		when(rs.getString(5)).thenReturn(testReimbursement1.getDescription());
		when(rs.getBlob(6)).thenReturn(testReimbursement1.getReceipe());
		when(rs.getInt(7)).thenReturn(testReimbursement1.getAuthorId());
		when(rs.getString(8)).thenReturn(testReimbursement1.getResolverUsername());
		when(rs.getString(9)).thenReturn(testReimbursement1.getStatus());
		when(rs.getString(10)).thenReturn(testReimbursement1.getType());
		when(rs.getString(11)).thenReturn(testReimbursement1.getUsername());
		when(ps.executeQuery()).thenReturn(rs);
		
	}
	
	//rs.getInt(1), rs.getInt(2), rs.getString(3),rs.getString(4), rs.getString(5), rs.getBlob(6), rs.getInt(7), managerUsername, status, type, username
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void testFindByNameSuccess() {
		assertEquals(new ReimbursementDao(rdc).getReimbursementById(1).getDescription(), testReimbursement1.getDescription());
	}

}
