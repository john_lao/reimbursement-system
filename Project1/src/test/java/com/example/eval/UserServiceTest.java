package com.example.eval;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.dao.ReimbursementDao;
import com.example.model.User;
import com.example.service.ReimbursementService;

public class UserServiceTest {
	@Mock
	private ReimbursementDao mockedDao;
	private ReimbursementService testService =  new ReimbursementService(mockedDao);
	private User testUser;
	
	@BeforeClass
	public static void setUpBeforeClass()  throws Exception {
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		testService = new ReimbursementService(mockedDao);
		testUser = new User(1, "shmoo", "testing", "john", "lao", "laojohnmatthew@gmail.com", "employee");
		when(mockedDao.findByUsername("shmoo")).thenReturn(testUser);
	}
	
	@After
	public void tearDown() throws Exception {
		
	}

	@Test
	public void testGetUseruccess() {
		assertEquals(testService.getUser("shmoo"), testUser);
	}
	
	@Test(expected = NullPointerException.class)
	public void testGetUserFailure() {
		assertEquals(testService.getUser("eqe3r"), null);
	}
	
	@Test
	public void testVerifyPasswordSuccess() {
		assertTrue(testService.passwordVerify("shmoo", "testing"));
	}
	
	@Test
	public void testVerifyPasswordFailure() {
		assertFalse(testService.passwordVerify("shmoo", "notCorrectPassword"));
	}



}