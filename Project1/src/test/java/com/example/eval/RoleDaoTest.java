package com.example.eval;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.dao.ReimbursementDBConnection;
import com.example.dao.ReimbursementDao;
import com.example.model.ReimbursementType;
import com.example.model.UserRole;

public class RoleDaoTest {
	private static String url = "jdbc:mariadb://database-1.c7otdgsixoyh.us-east-2.rds.amazonaws.com:3306/fooddb";
	private static String username = "fooduser";
	private static String password = "mypassword";
	
	@Mock
	private	ReimbursementDBConnection rdc;
	
//	@Mock
//	private DriverManager dm;
	
	@Mock
	private Connection c;
	
	@Mock
	private PreparedStatement ps;
	
	@Mock
	private ResultSet rs;
	
	private UserRole testRole;

	@BeforeClass
	public static void setUpBeforeClass()  throws Exception {
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(rdc.getDBConnection()).thenReturn(c);
		when(c.prepareStatement(any(String.class))).thenReturn(ps);
		testRole = new UserRole("employee", 1);
		
		when(rs.first()).thenReturn(true);
		when(rs.getInt(1)).thenReturn(testRole.getId());
		when(rs.getString(2)).thenReturn(testRole.getRole());

		when(ps.executeQuery()).thenReturn(rs);
		
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	
	@Test
	public void testRoleByIdSuccess() {
		assertEquals(new ReimbursementDao(rdc).getTypeById(1), "employee");
	}
	
	

}
