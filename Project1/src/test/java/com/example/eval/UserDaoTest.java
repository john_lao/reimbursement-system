package com.example.eval;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.dao.ReimbursementDBConnection;
import com.example.dao.ReimbursementDao;
import com.example.model.Reimbursement;
import com.example.model.User;

public class UserDaoTest {
	private static String url = "jdbc:mariadb://database-1.c7otdgsixoyh.us-east-2.rds.amazonaws.com:3306/fooddb";
	private static String username = "fooduser";
	private static String password = "mypassword";
	
	@Mock
	private	ReimbursementDBConnection rdc;
	
//	@Mock
//	private DriverManager dm;
	
	@Mock
	private Connection c;
	
	@Mock
	private PreparedStatement ps;
	
	@Mock
	private ResultSet rs;
	
	private User testUser;

	@BeforeClass
	public static void setUpBeforeClass()  throws Exception {
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(rdc.getDBConnection()).thenReturn(c);
		when(c.prepareStatement(any(String.class))).thenReturn(ps);
		testUser = new User(1, "shmoo", "testing", "john", "lao", "laojohnmatthew@gmail.com", "employee");
		
		when(rs.first()).thenReturn(true);
		when(rs.getInt(1)).thenReturn(testUser.getId());
		when(rs.getString(2)).thenReturn(testUser.getUsername());
		when(rs.getString(3)).thenReturn(testUser.getPassword());
		when(rs.getString(4)).thenReturn(testUser.getFirstName());
		when(rs.getString(5)).thenReturn(testUser.getLastName());
		when(rs.getString(6)).thenReturn(testUser.getEmail());
		when(rs.getString(7)).thenReturn(testUser.getRole());
		when(ps.executeQuery()).thenReturn(rs);
		
	}
	
	//rs.getInt(1), rs.getInt(2), rs.getString(3),rs.getString(4), rs.getString(5), rs.getBlob(6), rs.getInt(7), managerUsername, status, type, username
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void testFindByNameSuccess() {
		assertEquals(new ReimbursementDao(rdc).findByUsername("shmoo").getEmail(), testUser.getEmail());
	}
	

	@Test
	public void testFindUsernameByIdSuccess() {
		assertEquals(new ReimbursementDao(rdc).getUsernameById(1), testUser.getUsername());
	}
	

}
