package com.example.eval;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.dao.ReimbursementDao;
import com.example.model.Reimbursement;
import com.example.service.ReimbursementService;

public class ReimbursementServiceTest {
	@Mock
	private ReimbursementDao mockedDao;
	private ReimbursementService testService =  new ReimbursementService(mockedDao);
	private Reimbursement testReimbursement1;
	private Reimbursement testReimbursement2;
	private Reimbursement testReimbursement3;
	private Reimbursement testReimbursement4;
	private Reimbursement testReimbursement5;
	private List<Reimbursement> rAllList = new ArrayList<>();
	private List<Reimbursement> rUserList = new ArrayList<>();
	private List<Reimbursement> rInsertList = new ArrayList<>();
	
	@BeforeClass
	public static void setUpBeforeClass()  throws Exception {
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		testService = new ReimbursementService(mockedDao);
		testReimbursement1 = new Reimbursement(1,"approved", 4, "2021-01-09 10:29:16.0", "2021-01-09 19:29:16.0");
		testReimbursement2 = new Reimbursement(5, 300, "2021-01-09 10:29:16.0", "2021-01-09 19:29:16.0", 
				"food=20 per day, tranportation=50 total", null, 1, "smart", "approved", "food", "shmoo");
		testReimbursement3 = new Reimbursement(6, 300, "2021-01-09 10:29:16.0", "2021-01-09 19:29:16.0", 
				"food=20 per day, tranportation=50 total", null, 3, "smart", "approved", "food", "jacob");
		testReimbursement4 = new Reimbursement("food", 500,"i am describing","2021-01-09 10:29:16.0",1);
		testReimbursement5 = new Reimbursement("food", 0,"i am describing","2021-01-09 10:29:16.0",1);
		rUserList.add(testReimbursement1);
		rUserList.add(testReimbursement2);
		
		rAllList.add(testReimbursement1);
		rAllList.add(testReimbursement2);
		rAllList.add(testReimbursement3);
		rAllList.add(testReimbursement4);
		
		rInsertList.add(testReimbursement1);
		rInsertList.add(testReimbursement2);
		rInsertList.add(testReimbursement3);
		// int id, int amount, String dateSubmitted, String dateResolved, String description, Blob receipe,
		// int authorId, String resolverUsername, String status, String type, String username
		when(mockedDao.getAllReimbursementByUserId(1)).thenReturn(rUserList);
		when(mockedDao.getAllReimbursement()).thenReturn(rAllList);
		when(mockedDao.updateReimbursement(1,"approved", 4, "2021-01-09 10:29:16.0", "2021-01-09 19:29:16.0")).thenReturn(testReimbursement1);
		when(mockedDao.preparedInsertReimbursement("food", 500,"i am describing","2021-01-09 10:29:16.0",1)).thenReturn(rAllList);
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	

	@Test
	public void testGetUserReimbursementSuccess() {
		assertEquals(testService.getAllReimbursementByUserId(1), rUserList);
	}
	
	@Test
	public void testGetUserReimbursementFailure() {
		assertEquals(testService.getAllReimbursementByUserId(10), new ArrayList());
	}
	
	@Test
	public void testGetManagerReimbursementSuccess() {
		assertEquals(testService.getAllReimbursement(), rAllList);
	}
	
	@Test
	public void testUpdateReimbursementSuccess() {
		assertEquals(testService.updateReimbursementServer(new Reimbursement(1,"approved", 4, "2021-01-09 10:29:16.0", "2021-01-09 19:29:16.0")), testReimbursement1);
	}
	
	@Test
	public void testUpdateReimbursementFailure() {
		assertEquals(testService.updateReimbursementServer(new Reimbursement(1,"approved", 10, "2021-01-09 10:29:16.0", "2021-01-09 19:29:16.0")), null);
	}
	
	@Test
	public void testInsertReimbursementSuccess() {
		assertEquals(testService.insertReimbursementService(testReimbursement4), rAllList);
	}
	
	@Test
	public void testInsertReimbursementFailure() {
		assertEquals(testService.insertReimbursementService(testReimbursement5), null);
	}
	
}
