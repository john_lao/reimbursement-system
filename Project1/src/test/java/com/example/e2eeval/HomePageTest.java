package com.example.e2eeval;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.example.page.HomePage;
import com.example.page.LoginPage;

public class HomePageTest {
	HomePage homePage;
	LoginPage loginPage;
	private static WebDriver driver;
	
	@BeforeClass
	public static void setuUpBeforeClass() throws Exception{
		String filePath ="src/test/resources/chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", filePath);
		
		driver = new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	@AfterClass
	public static void tearDownAfterClass () throws Exception {
		driver.quit();
	}
	
	@Before
	public void setup() throws Exception {
		this.loginPage = new LoginPage(driver);
		loginPage.setUsername("smart");
		loginPage.setPassword("testing");
		loginPage.submit();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/home.html"));
		this.homePage = new HomePage(driver);
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void getHeader() {
		homePage.create();
		assertEquals(homePage.getFormHeader(), "Reimbursement Form");
	}
	
	@Test
	
	//able to insert reimbursement but cannot count the row
	// also need to know how to wait for ajax to complete and operate
	public void insertReimbursementSunccess() throws InterruptedException{
//		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("reimbursement_createdDate5")));
		int previous = homePage.getRow();
		System.out.println(homePage.getRow());
		homePage.create();
		homePage.setFormAmount("100");
		homePage.setFormDescription("desssss");
		homePage.setFormType("food");
		homePage.insertClick();
		WebDriverWait wait = new WebDriverWait(driver,80);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("reimbursement_createdDate"+ (homePage.getRow()))));
		homePage.setRow();
		assertEquals(homePage.getRow(), previous+1);
	}
	
	
	@Test
	public void LogoutSuccess() {
		homePage.logout();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/index.html"));
		assertEquals("http://localhost:9020/html/index.html", driver.getCurrentUrl());
	}
	
	@Test	
	// also need to know how to wait for ajax to complete and operate
	public void deniedClicked() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("reimbursement_createdDate"+ (homePage.getRow()-1))));
		homePage.DeniedClickById(homePage.getRow()-1);
		TimeUnit.SECONDS.sleep(30);
		assertEquals(homePage.getStatusOfElementId(homePage.getRow()-1), "denied");
	}
	
	@Test	
	// also need to know how to wait for ajax to complete and operate
	public void approvedClicked() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("reimbursement_createdDate"+ (homePage.getRow()-1))));
		homePage.ApprovedClickById(homePage.getRow()-1);
		TimeUnit.SECONDS.sleep(30);
		assertEquals(homePage.getStatusOfElementId(homePage.getRow()-1), "approved");
	}


}
