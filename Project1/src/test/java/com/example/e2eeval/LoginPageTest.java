package com.example.e2eeval;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.example.page.LoginPage;

public class LoginPageTest {

	LoginPage page;
		private static WebDriver driver;
		
		@BeforeClass
		public static void setuUpBeforeClass() throws Exception{
			String filePath ="src/test/resources/chromedriver.exe";
			System.setProperty("webdriver.chrome.driver", filePath);
			
			driver = new ChromeDriver();
			
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		}
		
		@AfterClass
		public static void tearDownAfterClass () throws Exception {
			driver.quit();
		}
		
		@Before
		public void setup() throws Exception {
			this.page = new LoginPage(driver);
		}
		
		@After
		public void tearDown() throws Exception {
			
		}
		
		
		@Test
		public void testSuccessfulLogin() {
			page.setUsername("smart");
			page.setPassword("testing");
			page.submit();
			WebDriverWait wait = new WebDriverWait(driver,60);
			wait.until(ExpectedConditions.urlMatches("/home.html"));
			assertEquals("http://localhost:9020/html/home.html", driver.getCurrentUrl());
		}
		
		@Test
		public void testFailedPassword() {
			page.setUsername("smart");
			page.setPassword("notCorrectPassword");
			page.submit();
			WebDriverWait wait = new WebDriverWait(driver,60);
			wait.until(ExpectedConditions.urlMatches("/WrongPassword.html"));
			assertEquals("http://localhost:9020/html/WrongPassword.html", driver.getCurrentUrl());
		}
		
		@Test
		public void testFailedUsername() {
			page.setUsername("noSuchUser");
			page.setPassword("notCorrectPassword");
			page.submit();
			WebDriverWait wait = new WebDriverWait(driver,60);
			wait.until(ExpectedConditions.urlMatches("/UnsuccessfulLogin.html"));
			assertEquals("http://localhost:9020/html/UnsuccessfulLogin.html", driver.getCurrentUrl());
		}
		
}