package com.example.dao;

public interface GenericDao <E>{
	public E findByUsername (String username);
	public void insert(E entity);
}
