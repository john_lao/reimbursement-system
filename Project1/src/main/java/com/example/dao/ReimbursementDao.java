package com.example.dao;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.example.model.Reimbursement;
import com.example.model.User;

public class ReimbursementDao implements GenericDao<User>{
	
	private ReimbursementDBConnection rdc;
	
	public ReimbursementDao() {
		super();
		// TODO Auto-generated constructor stub
	}
		
	public ReimbursementDao(ReimbursementDBConnection rdc) {
		super();
		this.rdc = rdc;
	}


	@Override
	public User findByUsername(String username) {
		// TODO Auto-generated method stub
			User u = null;
		try(Connection con = rdc.getDBConnection()){
			String sql = "SELECT * FROM sys_user WHERE username = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			
			if(!rs.first()) {
				return u;
			}
			String role = getRoleById(rs.getInt(7));
			u = new User(rs.getInt(1), rs.getString(2), rs.getString(3),rs.getString(4), rs.getString(5), rs.getString(6), role);
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return u;
	}
	
	
	public String getRoleById(int id) {
		String role = null;
		try(Connection con = rdc.getDBConnection()){
			String sql = "SELECT * FROM user_roles WHERE id = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			
			if(!rs.first()) {
				return role;
			}
			
			role = rs.getString(2);
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return role;
	}
	
	public String getUsernameById(int id) {
		String username = null;
		try(Connection con = rdc.getDBConnection()){
			String sql = "SELECT * FROM sys_user WHERE id = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			
			if(!rs.first()) {
				return username;
			}
			
			username = rs.getString(2);
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return username;
	}
	
	public String getStatusById(int id) {
		String status = null;
		try(Connection con = rdc.getDBConnection()){
			String sql = "SELECT * FROM reimbursement_status WHERE id = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			
			if(!rs.first()) {
				return status;
			}
			
			status = rs.getString(2);
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return status;
	}
	
	public int getIdByStatus(String status) {
		int id = -1;
		try(Connection con = rdc.getDBConnection()){
			String sql = "SELECT * FROM reimbursement_status WHERE reimbursement_status = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, status);
			ResultSet rs = ps.executeQuery();
			
			if(!rs.first()) {
				return id;
			}
			
			id = rs.getInt(1);
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return id;
	}
	
	public int getIdByType(String type) {
		int id = -1;
		try(Connection con = rdc.getDBConnection()){
			String sql = "SELECT * FROM reimbursement_type WHERE reimbursement_type = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, type);
			ResultSet rs = ps.executeQuery();
			
			if(!rs.first()) {
				return id;
			}
			
			id = rs.getInt(1);
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return id;
	}
	
	public String getTypeById(int id) {
		String Type = null;
		try(Connection con = rdc.getDBConnection()){
			String sql = "SELECT * FROM reimbursement_type WHERE id = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			
			if(!rs.first()) {
				return Type;
			}
			
			Type = rs.getString(2);
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return Type;
	}
	@Override
	public void insert(User entity) {
		// TODO Auto-generated method stub
		
	}
	
	
	// only used by getting employee
	public List<Reimbursement> getAllReimbursementByUserId(int Id) {
		// TODO Auto-generated method stub
		List<Reimbursement> rList = new ArrayList<>();
		try(Connection con = rdc.getDBConnection()){
			String sql = "SELECT * FROM reimbursement WHERE author_id = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, Id);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				String managerUsername = getUsernameById(rs.getInt(8));
				String status = getStatusById(rs.getInt(9));
				String type = getTypeById(rs.getInt(10));
				String username = getUsernameById(rs.getInt(7));
				Reimbursement r = new Reimbursement(rs.getInt(1), rs.getInt(2), rs.getString(3),rs.getString(4), rs.getString(5), rs.getBlob(6), rs.getInt(7), managerUsername, status, type, username);
				rList.add(r);
			}			
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return rList;
	}
	
	// manager - display all reimbursement when manager is accessing
	public List<Reimbursement> getAllReimbursement() {
		// TODO Auto-generated method stub
		List<Reimbursement> rList = new ArrayList<>();
		try(Connection con = rdc.getDBConnection()){
			String sql = "SELECT * FROM reimbursement";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				String username = getUsernameById(rs.getInt(7));
				System.out.println(username);
				String managerUsername = getUsernameById(rs.getInt(8));
				String status = getStatusById(rs.getInt(9));
				String type = getTypeById(rs.getInt(10));
				Reimbursement r = new Reimbursement(rs.getInt(1), rs.getInt(2), rs.getString(3),rs.getString(4), rs.getString(5), rs.getBlob(6), rs.getInt(7), managerUsername, status, type, username);
				rList.add(r);
			}			
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return rList;
	}
	
	
	public Reimbursement updateReimbursement(int id,String status,int userId,String date, String dateSubmitted) {
		// TODO Auto-generated method stub
		Reimbursement r = null;
		try(Connection con = rdc.getDBConnection()){
			String sql = "UPDATE reimbursement SET date_Resolved=?, resolver_id =?, status_id=?, date_submitted=? WHERE id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			int statusId = getIdByStatus(status);
		    ps.setString(1,date);
		    ps.setInt(2,userId);
		    ps.setInt(3,statusId);
		    ps.setString(4,dateSubmitted);
		    ps.setInt(5,id);
		    r = new Reimbursement(id,status, userId, date, dateSubmitted);
			ResultSet changed = ps.executeQuery();
			
			System.out.println(changed);
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		
		return r;
	}
	
	public Reimbursement getReimbursementById(int id) {
		// TODO Auto-generated method stub
		Reimbursement r = null;
		try(Connection con = rdc.getDBConnection()){
			String sql = "SELECT * from reimbursement WHERE id=?";
			PreparedStatement ps = con.prepareStatement(sql);

		    ps.setInt(1,id);
		    
		    ResultSet rs = ps.executeQuery();
		    
			while(!rs.first()) {
				return r;
			}
			String username = getUsernameById(rs.getInt(7));
			String managerUsername = getUsernameById(rs.getInt(8));
			String status = getStatusById(rs.getInt(9));
			String type = getTypeById(rs.getInt(10));
		    r = new Reimbursement(rs.getInt(1), rs.getInt(2), rs.getString(3),rs.getString(4), rs.getString(5), rs.getBlob(6), rs.getInt(7), managerUsername, status, type, username);
		    
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return r;
	}
	
	
	
	public List<Reimbursement> preparedInsertReimbursement(String type, int amount, String description, String dateSubmitted, int authorId) {
		try(Connection con = rdc.getDBConnection()){
			String sql = "INSERT INTO reimbursement(amount, date_submitted, description, author_id , type_id) VALUES(?,?,?,?,?)";
			int typeId = getIdByType(type);
			PreparedStatement prepStatement = con.prepareStatement(sql);
			prepStatement.setInt(1, amount);
			prepStatement.setString(2, dateSubmitted);
			prepStatement.setString(3, description);
			prepStatement.setInt(4, authorId);
			prepStatement.setInt(5, typeId);
			int changed = prepStatement.executeUpdate();
			System.out.println("Number of statement "+ changed);
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return getAllReimbursement();
	}
}
