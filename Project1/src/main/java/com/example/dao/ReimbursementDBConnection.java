package com.example.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ReimbursementDBConnection {
	private String url = "jdbc:mariadb://database-1.c0etbfnnye3r.us-east-2.rds.amazonaws.com:3306/reimbursementdb";
	private String username ="reimbursementuser";
	private String password ="mypassword";
	
	public Connection getDBConnection() throws SQLException {
		return DriverManager.getConnection(url,username,password);
	}
}
