package com.example;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.example.page.LoginPage;


public class SeleniumDriver {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		File file = new File("src/main/resources/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		
		WebDriver driver = new ChromeDriver();
		
//		driver.get("https://google.com");
//		
//		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
//		
//		WebElement searchBar = driver.findElement(By.name("q"));
//		WebElement searchButton = driver.findElement(By.name("btnK"));
//		
//		searchBar.sendKeys("Do a barrel roll");
//		searchButton.click();
//		
//		TimeUnit.SECONDS.sleep(10);
//		
//		driver.get("https://google.com");
//		
//		driver.quit();
		LoginPage page = new LoginPage(driver);
		page.navigateTo();
	}

}