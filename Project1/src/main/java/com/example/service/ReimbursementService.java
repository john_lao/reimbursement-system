package com.example.service;

import java.util.List;

import com.example.dao.ReimbursementDao;
import com.example.model.Reimbursement;
import com.example.model.User;

public class ReimbursementService {
	
	private ReimbursementDao rd = new ReimbursementDao();
	
	public ReimbursementService() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ReimbursementService(ReimbursementDao rd) {
		super();
		this.rd = rd;
	}
	
	public User getUser(String username) {
		User u = rd.findByUsername(username);
		
		if(u == null) {
			throw new NullPointerException();
		}	
		return u;
	}
	
	public boolean passwordVerify(String username, String password) {
		boolean isVerified =false;
		User u = getUser(username);
		if(u.getPassword().equals(password)) {
			isVerified = true;
		}
		return isVerified;
	}
	
	public List<Reimbursement> getAllReimbursementByUserId(int Id){
		List<Reimbursement> rList = rd.getAllReimbursementByUserId(Id);
		
		return rList;
	}
	
	public List<Reimbursement> getAllReimbursement(){
		List<Reimbursement> rList = rd.getAllReimbursement();
		return rList;
	}
	
	
	public Reimbursement updateReimbursementServer(Reimbursement r) {
		int id = r.getId();
		int ResolverId = r.getResolverId();
		String dateResolved = r.getDateResolved();
		String status = r.getStatus();
		String dateSubmitted = r.getDateSubmitted();
		return rd.updateReimbursement(id,status,ResolverId,dateResolved,dateSubmitted);
	}
	
	
	public List<Reimbursement> insertReimbursementService(Reimbursement r) {

		String type = r.getType();
		int amount = r.getAmount();
		String description = r.getDescription();
		String dateSubmitted = r.getDateSubmitted();
		int authorId = r.getAuthorId();
		if(amount <= 0) {
			return null;
		}else {
			return rd.preparedInsertReimbursement(type, amount, description, dateSubmitted, authorId);
		}
		// int typeId, int amount, String description, String dateSubmitted, int authorId
	}
	
	
	
	
}
