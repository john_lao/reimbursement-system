package com.example.controller;


import com.example.MainDriver;
import com.example.model.Reimbursement;
import com.example.model.User;
import com.example.service.ReimbursementService;

import io.javalin.http.Handler;

public class ReimbursementController {
	
	private ReimbursementService rServ = new ReimbursementService();
	public Handler postLogin = (ctx) -> {
//		System.out.println(ctx.body());
		
		if(rServ.passwordVerify(ctx.formParam("username"), ctx.formParam("password"))) {
			System.out.println("password is verified");
			ctx.sessionAttribute("user", rServ.getUser(ctx.formParam("username")));
			ctx.redirect("/html/home.html");
		}else {
			System.out.println("password is wrong");
			ctx.redirect("/html/WrongPassword.html");
		}
	};

	public ReimbursementController() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ReimbursementController(ReimbursementService rServ) {
		super();
		this.rServ = rServ;
	} 
	
	public Handler getSessUser = (ctx) ->{
		System.out.println((User)ctx.sessionAttribute("user"));
		System.out.println("line35");
		User u = (User)ctx.sessionAttribute("user");
		MainDriver.log.info(u.getUsername() + " is log in as" + u.getRole());
		ctx.json(u);
	};
	
	public Handler getReimbursement = (ctx) ->{
		User u = (User)ctx.sessionAttribute("user");
		if(u.getRole().equals("employee")) {
			ctx.json(rServ.getAllReimbursementByUserId(u.getId()));
		}else if(u.getRole().equals("finance manager")) {
			ctx.json(rServ.getAllReimbursement());
		}
	};
	
	
	public Handler postLogout = (ctx) ->{
		ctx.sessionAttribute("user",null);
		ctx.redirect("/html/index.html");
	};
	
	public Handler updateReimbursement = (ctx) ->{
		Reimbursement r = ctx.bodyAsClass(Reimbursement.class);
		rServ.updateReimbursementServer(r);
	};
	
	
	public Handler insertReimbursement = (ctx) ->{
		Reimbursement r = ctx.bodyAsClass(Reimbursement.class);
		rServ.insertReimbursementService(r);
	};
}
