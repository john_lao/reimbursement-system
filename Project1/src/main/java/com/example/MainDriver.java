package com.example;

import org.apache.log4j.Logger;

import com.example.controller.ReimbursementController;
import com.example.dao.ReimbursementDBConnection;
import com.example.dao.ReimbursementDao;
import com.example.service.ReimbursementService;

import io.javalin.Javalin;

public class MainDriver {
	
	public final static Logger log = Logger.getLogger(MainDriver.class);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		

		ReimbursementController rcon = new ReimbursementController(new ReimbursementService(new ReimbursementDao(new ReimbursementDBConnection()))); 

		
		Javalin app = Javalin.create(config->{
			config.addStaticFiles("FrontEnd");
		});
		
		app.start(9020);
		
		app.post("/users/login", rcon.postLogin);
		
		app.exception(NullPointerException.class, (e,ctx) ->{
			ctx.status(404);
			ctx.redirect("/html/UnsuccessfulLogin.html");
		});
		
		app.get("/users/session", rcon.getSessUser);
		
		app.get("/user/reimbursement", rcon.getReimbursement);
		
		app.post("/users/logout", rcon.postLogout);
		
		app.put("/users/reimbursement", rcon.updateReimbursement);
		
		app.post("/users/reimbursement", rcon.insertReimbursement);
		
	}

}

