package com.example.model;

public class ReimbursementStatus {
	private String status;
	private int id;
	public ReimbursementStatus() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ReimbursementStatus(String status, int id) {
		super();
		this.status = status;
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public int getId() {
		return id;
	}
	@Override
	public String toString() {
		return "ReimbursementType [status=" + status + ", id=" + id + "]";
	}
	
}
