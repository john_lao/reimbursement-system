package com.example.model;

import java.sql.Blob;

public class Reimbursement {
	private int id;
	private int amount;
	private String dateSubmitted;
	private String dateResolved;
	private String description;
	private int authorId;
	private String resolverUsername;
	private String status;
	private String type;
	private Blob receipe;
	private int resolverId;
	private String username;
	
	public Reimbursement() {
		super();
		// TODO Auto-generated constructor stub
	}
		
	
	public Reimbursement(int id, String status, int resolverId, String dateResolved, String dateSubmitted) {
		super();
		this.id = id;
		this.dateResolved = dateResolved;
		this.resolverId = resolverId;
		this.status = status;
		this.dateSubmitted = dateSubmitted;
	}
	
	
	public Reimbursement(String type, int amount, String description, String dateSubmitted, int authorId) {
		super();
		this.type = type;
		this.dateSubmitted = dateSubmitted;
		this.description = description;
		this.amount = amount;
		this.authorId = authorId;
	}
	
	public Reimbursement(int id, int amount, String dateSubmitted, String dateResolved, String description, Blob receipe,
			int authorId, String resolverUsername, String status, String type) {
		super();
		this.id = id;
		this.amount = amount;
		this.dateSubmitted = dateSubmitted;
		this.dateResolved = dateResolved;
		this.description = description;
		this.receipe = receipe;
		this.authorId = authorId;
		this.resolverUsername = resolverUsername;
		this.status = status;
		this.type = type;
	}

	public Reimbursement(int id, int amount, String dateSubmitted, String dateResolved, String description, Blob receipe,
			int authorId, String resolverUsername, String status, String type, String username){
		super();
		this.id = id;
		this.amount = amount;
		this.dateSubmitted = dateSubmitted;
		this.dateResolved = dateResolved;
		this.description = description;
		this.receipe = receipe;
		this.authorId = authorId;
		this.resolverUsername = resolverUsername;
		this.status = status;
		this.type = type;
		this.username = username;
	}

	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getDateSubmitted() {
		return dateSubmitted;
	}

	public void setDateSubmitted(String dateSubmitted) {
		this.dateSubmitted = dateSubmitted;
	}

	public String getDateResolved() {
		return dateResolved;
	}

	public void setDateResolved(String dateResolved) {
		this.dateResolved = dateResolved;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	public Blob getReceipe() {
		return receipe;
	}

	public void setReceipe(Blob receipe) {
		this.receipe = receipe;
	}

	
	public int getAuthorId() {
		return authorId;
	}

	public void setAuthorId(int authorId) {
		this.authorId = authorId;
	}



	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public String getResolverUsername() {
		return resolverUsername;
	}

	public void setResolverUsername(String resolverUsername) {
		this.resolverUsername = resolverUsername;
	}
	
	

	public int getResolverId() {
		return resolverId;
	}


	@Override
	public String toString() {
		return "Reimbursement [id=" + id + ", amount=" + amount + ", dateSubmitted=" + dateSubmitted + ", dateResolved="
				+ dateResolved + ", description=" + description + ", authorId=" + authorId + ", resolverUsername="
				+ resolverUsername + ", status=" + status + ", type=" + type + ", receipe=" + receipe + ", resolverId="
				+ resolverId + "]";
	}
	
	
}
