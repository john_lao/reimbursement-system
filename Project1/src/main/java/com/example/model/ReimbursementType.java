package com.example.model;

public class ReimbursementType {
	
	private String type;
	private int id;
	public ReimbursementType() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ReimbursementType(String type, int id) {
		super();
		this.type = type;
		this.id = id;
	}
	public String gettype() {
		return type;
	}
	public int getId() {
		return id;
	}
	@Override
	public String toString() {
		return "ReimbursementType [type=" + type + ", id=" + id + "]";
	}
	
	
}
