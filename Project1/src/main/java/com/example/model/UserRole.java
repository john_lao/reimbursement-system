package com.example.model;

public class UserRole {
	private String role;
	private int id;
	public UserRole() {
		super();
		// TODO Auto-generated constructor stub
	}
	public UserRole(String role, int id) {
		super();
		this.role = role;
		this.id = id;
	}
	public String getRole() {
		return role;
	}
	public int getId() {
		return id;
	}
	@Override
	public String toString() {
		return "UserRole [role=" + role + ", id=" + id + "]";
	}
	
	
}
