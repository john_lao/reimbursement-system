package com.example.page;

import java.awt.Dimension;
import java.io.Console;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;



public class HomePage {
	
	private WebDriver driver;
	private WebElement createButton;
	private WebElement logoutButton;
	private WebElement selector;
	private WebElement formHeader;
	private Select formType;
	private WebElement formAmount;
	private WebElement formDescription;
	private WebElement insertSubmit;
	private List<WebElement> row;
	private org.openqa.selenium.Dimension table;
	private WebElement deniedButton;
	private WebElement approvedButton;
	public HomePage(WebDriver driver) throws InterruptedException {
		this.driver = driver;
		
		this.createButton = driver.findElement(By.name("create-reimbursement"));
		this.logoutButton = driver.findElement(By.name("logout"));
		this.selector = driver.findElement(By.name("status-selector"));
		this.formHeader = driver.findElement(By.tagName("h5"));
		this.formType = new Select(driver.findElement(By.id("type-select")));
		this.formAmount = driver.findElement(By.id("exampleInputAmount"));
		this.formDescription = driver.findElement(By.id("exampleInputDescription"));
		this.insertSubmit = driver.findElement(By.id("insert-submit"));
//		this.row = driver.findElements(By.xpath("/html/body/form/div[4]/div/table/tbody/tr"));
		TimeUnit.SECONDS.sleep(35);
		this.row = driver.findElements(By.xpath("//*[@id='reimbursement-table']/tbody/tr"));
		this.table = driver.findElement(By.id("reimbursement-table")).getSize();
			
	}

	
	
	public int getRow() {
//		System.out.println(this.table.getWidth());
//		return this.table.getHeight();
		return this.row.size();
	}
	
	public void setRow() {
		this.row = driver.findElements(By.xpath("//*[@id='reimbursement-table']/tbody/tr"));
	}
	
	
	public void DeniedClickById(int id) {
		this.deniedButton = driver.findElement(By.id("denied-button"+id));
		System.out.println(this.deniedButton);
		this.deniedButton.click();
	}
	
	public void ApprovedClickById(int id) {
		this.approvedButton = driver.findElement(By.id("approved-button"+id));
		System.out.println(this.approvedButton.getAttribute("value"));
		this.approvedButton.click();
	}
	
	public String getStatusOfElementId(int id) {
		return driver.findElement(By.id("reimbursement_status"+id)).getText();
	}
	
	public void setFormType(String type) {
		this.formType.selectByVisibleText(type);
	}
	
	public void setFormAmount(String amount) {
		this.formAmount.clear();
		this.formAmount.sendKeys(amount);
	}
	
	public void setFormDescription(String des) {
		this.formDescription.clear();
		this.formDescription.sendKeys(des);
	}

	public void insertClick() {
		this.insertSubmit.click();
	}
	
	public void create() {
		this.createButton.click();
	}
	
	public String getFormHeader() {
		return this.formHeader.getText();
	}
	
	public void logout() {
		this.logoutButton.click();
	}
	
	public String getFilterValue() {
		return this.selector.getAttribute("value");
	}
};
