package com.example.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {
	
	private WebDriver driver;
	private WebElement header;
	private WebElement usernameField;
	private WebElement passwordField;
	private WebElement mySubmitButton;
	
	public LoginPage(WebDriver driver) {
		this.driver = driver;
		this.navigateTo();
		
		this.usernameField = driver.findElement(By.name("username"));
		this.passwordField = driver.findElement(By.name("password"));
		this.mySubmitButton = driver.findElement(By.name("user-submit"));
	}
	
	public void setUsername(String name) {
		this.usernameField.clear();
		this.usernameField.sendKeys(name);
	}
	
	public String getUsername() {
		return this.passwordField.getAttribute("value");
	}
	
	public void setPassword(String power) {
		this.passwordField.clear();
		this.passwordField.sendKeys(power);
	}
	
	public String getPassword() {
		return this.passwordField.getAttribute("value");
	}
	
	public String getHeader() {
		return this.header.getText();
	}
	
	public void submit() {
		this.mySubmitButton.click();
	}
	
	
	public void navigateTo() {
		this.driver.get("http://localhost:9020/html/index.html");
	}
}