#ERS_REIMBURSEMENT_SYSTEM

## Project Description
The Expense Reimbursement System (ERS) will manage the process of reimbursing employees for expenses incurred while on company time. All employees in the company can login and submit requests for reimbursement and view their past tickets and pending requests. Finance managers can log in and view all reimbursement requests and past history for all employees in the company. Finance managers are authorized to approve and deny requests for expense reimbursement. ERS practice test-driven development whereas frontend service are end to end tested via Selenium with Junit while backend are unit tested via Junit.

## Technologies Used
* Javalin - Version 3.10.1
* slf4j - Version 1.7.30
* Log4J - Version 1.2.17
* Java - Version 1.8
* Selenium Version - 3.141.59
* Jackson - Version -2.10.3
* Junit - Version 4.12
* Mockito - Version 1.10.19
* Mariadb - 2.6.2

## Features
* Redirect user to different pages if users does not exist or password is incorrect 
* Able to log into an employee page and submit reimbursements
* Able to log into a manager page and submit reimbursements, and able to approve/deny pending reimbursements, able to approve denied reimbursement
* Able to filter reimbursement by status

To-do list:
* Refactor the backend into using Hibernate, to minimizing the query time of performing the CRUD operation
* Fix Logout issue so that users credential is removed after pressing the back button
* Add Functionality for creating employee users
* Add functionality for uploading a document or image of their receipt when submitting reimbursements
* Encrypted the password in Java and securely stored in the database 



## Getting Started
Here are the step to start the Service:

Step1:
Open the terminal, and enter git clone https://gitlab.com/john_lao/reimbursement-system.git

Step2:
Start the application by navigating to src/main/java/com/example 

Step3:
Enter the MainDriver.java and start the server

Step4:
Open the browser (works in any browser) and the service will be available at http://localhost:9020/html/index.html


## Usage
After the application has started, you can log into the reimbursement system as an Employee or as a Financial Manager.
You will be able to submit reimbursements when click create reimbursement button. After the reimbursement have been created the new reimbursement will be listed on the table. 
You will be able to filter the reimbursement by the status of the reimbursement
You will be able to approve/deny reimbursements the pending reimbursement and approve the denied reimbursement if you are the financial manager. 
Logging information will be found in the log4j-application.log and STS's console
Start the tests by right-clicking src/test/java and run as Junit test.







